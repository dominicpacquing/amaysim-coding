# AMAYSIM

## Overview
The special offer and promotions are divided into two types: product and pricing.

* Pricing - this type of rule applies to anything that affects the amount of the cart.

	* Group Deal (`rule/price/group_deal.rb`)
          *  item - product that will be used as a reference for the discount.
          *  group_count - specifies the number of items required before applying the discount.
          *  group\_cut\_count - multiplier for the item's price based on every matched group_count.


	   			Rule::Price::GroupDeal.new(ult_small, 3, 2)

	* Code Deal (`rule/price/code.rb`)
		* code - text that will	 be used as a reference for the discount
		* percentage - percentage of the discount

				Rule::Price::Code.new('1<3AMAYSIM', 10)

	* Bulk Discount (`rule/price/bulk_discount.rb`)
		* item - product that will be used as a reference for the discount.
		* required\_count - the number of products required before the discount_price is applied
		* discount\_price - replaces the original price of the item once the required_count is met.  

				Rule::Price::Discount.new(ult_large, 4, 39.9)

* Product - this type of rule applies to anything that alters the number of items in the cart.

	* Free bundle(`rule/product/free_bundle.rb`)
		* item - product that will be used as a reference for the discount.
		* bonus_item	- for every matched item, add this bonus item to cart



				Rule::Product::FreeBundle.new(ult_small, one_gb)

### Requirements
* Ruby - 2.X - The specific version used for this project is `2.4.0` as indicated in the `.ruby-version` file.

### Install gems
1. Install bundler

		gem install bundler

2. Install project dependencies

		bundle install


### Run tests

After installing the project dependencies, you may run the tests by executing the ff command:


		bundle exec rspec


### Run the application via irb

1. Load the irb, add lib folder to load path, and load the base file

		irb -I lib -r amaysim-app

2. Run sample application

		# Instantiate the products
		ult_large = Item.new('ult_large', 'Unlimited 5GB', 44.9)
		ult_medium = Item.new('ult_medium', 'Unlimited 2GB', 29.9)
		ult_small = Item.new('ult_small', 'Unlimited 1GB', 24.9)
		one_gb = Item.new('1gb', '1 GB Data-pack', 9.90)

		# Create bulk discount rule
		rule = Rule::Price::BulkDiscount.new(ult_large, 4, 39.9)

		# Create sample list of products
		orders = ([ ult_small ] * 2) + ([ ult_large ] * 4)

		# Instantiate the shopping cart
		shopping_cart = ShoppingCart.new([ rule ])

		# Add the items to cart
		orders.shuffle.each {|i| shopping_cart.add(i)}

		shopping_cart.total #=> 209.4

		shopping_cart.items #=> [
		 "Unlimited 1GB",
		 "Unlimited 5GB",
		 "Unlimited 5GB",
		 "Unlimited 1GB",
		 "Unlimited 5GB",
		 "Unlimited 5GB"
		]
