require 'item'
require 'rule/product/free_bundle'

describe Rule::Product::FreeBundle do
  let(:ult_medium) { Item.new('ult_medium', 'Unlimited 2GB', 29.9) }
  let(:ult_small) { Item.new('ult_small', 'Unlimited 1GB', 24.9) }
  let(:one_gb) { Item.new('1gb', '1 GB Data-pack', 9.90) }
  let(:rule) { Rule::Product::FreeBundle.new(ult_small, one_gb) }

  it 'should return bonus items if orders matched' do
    orders = [ ult_small ] * 3
    expect(rule.apply(orders)).to eql([one_gb] * 3)
  end

  it 'should not apply discount if number of items is not sufficient' do
    orders = [ ult_medium, ult_medium ]
    expect(rule.apply(orders)).to eql([])
  end
end
