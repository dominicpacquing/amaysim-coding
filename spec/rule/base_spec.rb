require 'rule/base'

describe Rule::Base do
  it 'should raise error when calling apply' do
    expect { subject.apply }.to raise_error(/its own implementation/)
  end
end
