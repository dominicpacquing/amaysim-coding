require 'item'
require 'rule/price/code'

describe Rule::Price::Code do
  let(:code) { '1<3AMAYSIM' }
  let(:orders) { [ Item.new('ult_small', 'Unlimited 1GB', 24.9), Item.new('1gb', '1 GB Data-pack', 9.90) ] }
  let(:rule) { Rule::Price::Code.new(code, 10) }

  it 'should apply total discount if code matched' do
    orders_total = orders.inject(0){|total, item| total + item.price }
    expect(rule.apply(orders, code)).to eql(orders_total * rule.percentage / 100)
  end

  it 'should not apply discount if code did not match' do
    expect(rule.apply(orders, 'abc')).to eql(0)
  end
end
