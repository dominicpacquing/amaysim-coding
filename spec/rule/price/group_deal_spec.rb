require 'item'
require 'rule/price/group_deal'

describe Rule::Price::GroupDeal do
  let(:ult_small) { Item.new('ult_small', 'Unlimited 1GB', 24.9) }
  let(:rule) { Rule::Price::GroupDeal.new(ult_small, 3, 2) }

  it 'should apply discount if there is enough number of items' do
    orders = [ ult_small ] * rule.group_count
    expect(rule.apply(orders)).to eql(ult_small.price)
  end

  it 'should apply discount for each group deal' do
    orders = [ ult_small ] * (rule.group_count * rule.group_cut_count + 1)
    expect(rule.apply(orders)).to eql(ult_small.price * 2)
  end

  it 'should not apply discount if number of items is not sufficient' do
    orders = [ ult_small, ult_small ]
    expect(rule.apply(orders)).to eql(0.0)
  end
end
