require 'item'
require 'rule/price/bulk_discount'

describe Rule::Price::BulkDiscount do
  let(:ult_large) { Item.new('ult_large', 'Unlimited 5GB', 44.9) }
  let(:ult_small) { Item.new('ult_small', 'Unlimited 1GB', 24.9) }
  let(:rule) { Rule::Price::BulkDiscount.new(ult_large, 4, 39.9) }

  it 'should calculate the discount for matched item' do
    orders = ([ult_large] * rule.required_count) + ([ult_small] * 2)
    expect(rule.apply(orders)).to be == (ult_large.price - rule.discount_price) * rule.required_count
  end

  it 'should have the required number of items before applying discount' do
    ult_large_count = rule.required_count - 1
    orders = ([ult_large] * ult_large_count) + ([ult_small] * 2)
    expect(rule.apply(orders)).to be(0)
  end

  it 'should not apply discount if item did not match' do
    orders = [ult_small] * 2
    expect(rule.apply(orders)).to be(0)
  end
end
