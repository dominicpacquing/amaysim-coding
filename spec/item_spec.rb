require 'item'

describe Item do
  subject { Item.new('ult_large', 'Unlimited 5GB', 44.9) }
  it { should respond_to :price }
  it { should respond_to :code }
  it { should respond_to :name }
end
