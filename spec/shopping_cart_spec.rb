require 'shopping_cart'
require 'rule/price/bulk_discount'
require 'rule/price/code'
require 'rule/price/group_deal'
require 'rule/product/free_bundle'

describe ShoppingCart do
  let(:ult_large) { Item.new('ult_large', 'Unlimited 5GB', 44.9) }
  let(:ult_small) { Item.new('ult_small', 'Unlimited 1GB', 24.9) }
  let(:one_gb) { Item.new('1gb', '1 GB Data-pack', 9.90) }
  let(:ult_medium) { Item.new('ult_medium', 'Unlimited 2GB', 29.9) }

  context '#total' do
    context 'price rule' do
      it 'should apply group deal rule' do
        ult_small_items = [ ult_small ] * 3
        ult_large_items = [ ult_large ]
        items = ult_small_items + ult_large_items

        rules = [ Rule::Price::GroupDeal.new(ult_small, 3, 2) ]
        shopping_cart = ShoppingCart.new(rules)
        items.shuffle.each {|i| shopping_cart.add(i)}

        expect(shopping_cart.total).to eql(94.7)
      end

      it 'should apply bulk discount rule' do
        ult_small_items = [ ult_small ] * 2
        ult_large_items = [ ult_large ] * 4
        items = ult_small_items + ult_large_items

        rules = [ Rule::Price::BulkDiscount.new(ult_large, 4, 39.9) ]
        shopping_cart = ShoppingCart.new(rules)
        items.shuffle.each {|i| shopping_cart.add(i)}

        expect(shopping_cart.total).to eql(209.4)
      end

      it 'should apply Rule::Price::Code' do
        code = '1<3AMAYSIM'
        items = [ ult_small, one_gb ]

        rules = [ Rule::Price::Code.new(code, 10) ]
        shopping_cart = ShoppingCart.new(rules)
        shopping_cart.add(ult_small)
        shopping_cart.add(one_gb, code)

        expect(shopping_cart.total).to eql(31.32)
      end
    end

    context 'product rule' do
      context 'free bundle' do
        it 'should not change the total price' do
          items = [ ult_small, ult_medium, ult_medium ]
          rules = [ Rule::Product::FreeBundle.new(ult_medium, one_gb) ]
          shopping_cart = ShoppingCart.new(rules)
          items.shuffle.each {|i| shopping_cart.add(i)}

          expect(shopping_cart.total).to eql(84.7)
        end
      end
    end
  end

  context '#items' do
    context 'product rule' do
      context 'free bundle' do
        it 'should bundle other item' do
          items = [ ult_small, ult_medium, ult_medium ]
          rules = [ Rule::Product::FreeBundle.new(ult_medium, one_gb) ]
          shopping_cart = ShoppingCart.new(rules)
          items.shuffle.each {|i| shopping_cart.add(i)}
          expected_items = items + [ one_gb, one_gb ]
          expect(shopping_cart.items).to match_array( expected_items.map(&:name) )
        end
      end
    end
  end
end
