class ShoppingCart
  attr_reader :pricing_rules, :product_rules, :products, :promo_code

  def initialize(rules)
    @pricing_rules, @product_rules = rules.partition { |r| r.type == 'price' }
    @products = []
  end

  def add(item, code = nil)
    products << item
    @promo_code = code unless code.nil?
  end

  def total
    total = products.inject(0) { |t, i| t + i.price }
    total_discount = pricing_rules.inject(0) do |t, rule|
      t + rule.apply(products, promo_code)
    end
    (total - total_discount).round(2)
  end

  def items
    total_items = products + product_rules.inject([]) do |t, rule|
      t + rule.apply(products, promo_code)
    end
    total_items.map(&:name)
  end
end
