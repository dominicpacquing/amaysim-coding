require 'rule/base'

module Rule
  module Price
    class Base < ::Rule::Base
      def type
        'price'
      end
    end
  end
end
