require 'rule/price/base'

module Rule
  module Price
    class Code < Base
      attr_reader :code, :percentage

      def initialize(code, percentage)
        @code = code
        @percentage = percentage
      end

      def apply(items, user_code)
        if user_code == code
          discount_percent = percentage / 100.0
          items.inject(0) { |total, item| total + item.price } * discount_percent
        else
          0
        end
      end
    end
  end
end
