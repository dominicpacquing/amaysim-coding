require 'rule/price/base'

module Rule
  module Price
    class BulkDiscount < Base
      attr_reader :product, :required_count, :discount_price

      def initialize(product, required_count, discount_price)
        @product = product
        @required_count = required_count
        @discount_price = discount_price
      end

      def apply(items = [], _ = nil)
        matched_items = items.select { |item| item.code == product.code }
        matched_items.length >= required_count ? (product.price - discount_price) * matched_items.length : 0
      end
    end
  end
end
