require 'rule/price/base'

module Rule
  module Price
    class GroupDeal < Base
      attr_reader :item, :group_count, :group_cut_count

      def initialize(item, group_count, group_cut_count)
        @item = item
        @group_count = group_count
        @group_cut_count = group_cut_count
      end

      def apply(items, _ = nil)
        items_by_group = items.group_by(&:code)
        target_item_group = items_by_group[item.code] || []
        ngroups = target_item_group.length / group_count
        (group_count - group_cut_count) * ngroups * item.price
      end
    end
  end
end
