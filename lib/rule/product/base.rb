require 'rule/base'

module Rule
  module Product
    class Base < ::Rule::Base
      def type
        'product'
      end
    end
  end
end
