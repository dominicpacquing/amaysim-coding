require 'rule/product/base'

module Rule
  module Product
    class FreeBundle < Base
      attr_reader :item, :bonus_item, :group_count

      def initialize(item, bonus_item, group_count = 1)
        @item = item
        @group_count = group_count
        @bonus_item = bonus_item
      end

      def apply(items, _ = nil)
        items_by_group = items.group_by(&:code)
        target_item_group = items_by_group[item.code] || []
        ngroups = target_item_group.length / group_count
        ([ bonus_item ] * ngroups).map do |b_item|
          b_item.price = 0.0
          b_item
        end
      end
    end
  end
end
